# vue_trivia

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Triva Game
Each trivia game will consist of 10 questions. Every correct answer gives 10 points. Your answer will be showed alongside the correct answer in the end of the game. 
